package br.senai.sp.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import br.senai.sp.modelo.SubTarefa;
import br.senai.sp.modelo.Tarefa;

@Repository
public class DAOSubTarefa {
	@PersistenceContext
	private EntityManager manager;

	@Transactional
	public void inserir(Long idTarefa, SubTarefa subTarefa) {
		Tarefa tarefa = manager.find(Tarefa.class, idTarefa);
		subTarefa.setTarefa(tarefa);
		manager.persist(subTarefa);
	}

	public SubTarefa buscar(Long id) {
		return manager.find(SubTarefa.class, id);
	}

	@Transactional
	public void marcarFeita(Long idSub, boolean valor) {
		SubTarefa subTarefa = buscar(idSub);
		subTarefa.setFeita(valor);
		manager.merge(subTarefa);
	}

	@Transactional
	public void excluir(Long idSub) {
		SubTarefa subTarefa = buscar(idSub);

		Tarefa tarefa = subTarefa.getTarefa();

		tarefa.getSubTarefas().remove(subTarefa);

	}
}
